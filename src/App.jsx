
import React, { useEffect, useState } from "react";

import Routing from "./components/organisms/RoutingPages";

import Header from "../src/components/organisms/header";

import "./css/style.css";

export default function App() {
  // Status acts as a traffic light controller 🚦 for our internet access
  // 0 is loading, 1 is ready, 2 is error.
  const [status, setStatus] = useState(0);

  const [information, setInformation] = useState([]);
  const endpoint = "https://my.api.mockaroo.com/orders.json?key=e49e6840";

  /* const MultipleOrders = information.map((item) => {

    return <Card orders= {item} />;
  });

  const OrderDetails = information.map((item) => {
    return <ParcelPage orders={item} />;
  }); */


  // Contructor like hook
  useEffect(() => {
    const getData = async () => {
      try {
        // Make fetch request to obtain info frmo the endpoint URL address
        // Cors is a security measure.
        const response = await fetch(endpoint, { mode: "cors" });

        // Once the information is downloaded we transformed it to json
        const orders = await response.json();

        // Once the information is transformed to json we put it into our hooks
        setInformation(orders);

        // Once we have the information in the hook we give the ok! to change the status
        setStatus(1);
      } catch {
        setStatus(2);
      }
    };

    getData();
  }, []); // this last [] is cruccial otherwise you will call over and over the server and crash it

  

  return (
    <div className="home-page">
      <header>
      <Header />
      </header>
      {status === 0 ? <p>Loading...</p> : null}
      {status === 1 ? <Routing orders={information} /> : null}
      {status === 2 ? <p>Sorry we cannot find data</p> : null}
    </div>
  );
}
