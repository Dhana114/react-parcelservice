import React from "react";
import { Link, useParams } from "react-router-dom";
import { useHistory } from 'react-router';

import Description from "../molecules/Descriptions";

export default function DetailView({ orders }) {
  const { parcel_id } = useParams();
  const { push } = useHistory();
  const selectedOrder = orders.find((order) => order.parcel_id === parcel_id);
  const {
    eta,
    last_updated,
    location_name,
    notes,
    sender,
    verification_required,
  } = selectedOrder;

  return (
    <section className="page-center">
      <header className="parcel-header">
        <h1 className="parcel-heading">
         Orders from {sender}
        </h1>
        <h3>Information available on your order</h3>
      </header>

      <div >
        

        <div className="page-content" >
          <article>
            <Description
              label="Location"
              text={location_name}
            />
            <Description
              label="Estimated Time of Delivery"
              text={eta}
            />
            <Description
              label="Last_Update"
              text={last_updated}
            />
            {notes ? (
              <Description label="Note" text={notes} />
            ) : (
              ""
            )}
            {verification_required ? (
              <Description
                label="Verification"
                text="Need Verification for this Package"
              />
            ) : (
              ""
            )}
          </article>
        </div>
        <div className="button_margin">
        <button className="back-button"
          type="button"
        onClick={() => push('/')}
        >
        Go Back
        </button>
        </div>
      </div>

      
    </section>
  );
}

