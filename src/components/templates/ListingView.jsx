import React from "react";
import ListofOrders from "../organisms/ListOfOrders";

export default function ListView({ orders }) {
  

  return (
    <div className="home-page"  >
      <header className="homePage-content">
        <h1 className="homePage-header">
         Hey {orders[0].user_name} !
        </h1>
        <p className="homePage-des"> Your Parcels</p>
      </header>

      <ListofOrders orders={orders} />
    </div>
  );
}


