import React from "react";
import {BrowserRouter as Router,  Switch, Route } from "react-router-dom";
import ListView from "../templates/ListingView";
import DetailView from "../templates/DetailParcels";

export default function MountedComponent({ orders }) {
  return (
      <Router>
    <Switch>
      <Route path="/parcel/:parcel_id">
        <DetailView orders={orders} />
      </Route>
      <Route path="/">
        <ListView orders={orders} />
      </Route>
    </Switch>
    </Router>
  )
}

