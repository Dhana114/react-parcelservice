import React from "react";
import AllCards from "../molecules/AllCards";

export default function OrderList({ orders }) {
  const listItems = orders.map((item) => {
    return <AllCards key={item.parcel_id} order={item} />;
  });

  return <div className="orders-list">{listItems}</div>;
}

