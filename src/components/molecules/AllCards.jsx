import React from "react"
import { Link } from "react-router-dom"

export default function OrderItem({ order }) {
  const { parcel_id, sender, status } = order;
  

  return (
    


  <div className="card">
          <h3 >{sender}</h3>
            <p className="title">
           Parcel <span className="para-font">#{parcel_id}</span>
            </p>
            <p > {status} </p>
            <Link className="button" to={`/parcel/${parcel_id}`}>
          View Details
        </Link>
        </div> 
     
      

    
  )
}


